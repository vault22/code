#include <stdio.h>

int main() {
  int piecesOfPizza = 5;
  int numberOfEaters = 2;
  int leftOver = piecesOfPizza % numberOfEaters;

  printf("The leftover pizza amount is: %i\n", leftOver);

  return 0;
}