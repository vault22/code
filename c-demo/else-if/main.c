// Doctors office
// 1 add patient
// 2 view patients
// 3 search patients
// 4 exit
#include <stdio.h>

int main() {
  printf("Choose a menu option between 1-4\n");
  printf("1. Add a patient\n");
  printf("2. View a patient\n");
  printf("3. Search for a patient\n");
  printf("4. Exit application\n");

  int input;
  printf(">>>: ");
  scanf("%d", &input);

  switch(input)
  {
    case 1:
      printf("Adding a patient\n");
      break;
    case 2:
      printf("Viewing a patient\n");
      break;
    case 3:
      printf("Searching for a patient\n");
      break;
    case 4:
      printf("Exiting application...\n");
      break;
    default:
      printf("Invalid choice, enter a number between 1 to 4.\n");
      break;
  }

  return 0;
}