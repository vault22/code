#include <ncurses.h>
#include <stdlib.h>
#include <time.h>

#define BAR_LENGTH 5

void draw_bar(WINDOW *win, int starty, int startx);

int main() {
    int ch;
    int bar_y = 10;

    initscr();
    timeout(0); // Make getch() non-blocking
    raw();
    keypad(stdscr, TRUE);
    noecho();

    // Create the window for the pong bar
    WINDOW *bar_win = newwin(BAR_LENGTH, 1, bar_y, 0);

    draw_bar(bar_win, bar_y, 0);

    while ((ch = getch()) != 'q') {
        if (ch == KEY_UP) {
            bar_y--;
        } else if (ch == KEY_DOWN) {
            bar_y++;
        }
        wmove(stdscr, 0, 0);
        wrefresh(stdscr);
        mvwin(bar_win, bar_y, 0);
        draw_bar(bar_win, bar_y, 0);
        napms(20);
    }

    endwin();
    return 0;
}

void draw_bar(WINDOW *win, int starty, int startx) {
    wmove(win, 0, 0);
    for (int i = 0; i < BAR_LENGTH; i++) {
        mvwaddch(win, i, startx, ACS_BLOCK);
    }
    wrefresh(win);
}
