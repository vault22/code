/*****************
Description: Tool to calculate ingredients required for Kombucha brewing.
*****************/
#include <stdio.h>

int main() {
  int boochLiters;
  int sugarGrams = (boochLiters * 70);             // Ratio is 70g per liter
  int teaGrams = (boochLiters * 7);               //  Ratio is 7g per liter
  int boochMilliliters = (boochLiters / 4) * 200;  // 200ml booch per 4L

  printf("Liters of booch to create: ");
  scanf("%i", &boochLiters);

  printf("The amount of sugar needed is: %i grams\n", sugarGrams);
  printf("The amount of tea requried is: %i grams\n", teaGrams);
  printf("The required amount of kombucha starter culture is: %i milliliters", boochMilliliters);

  return 0;
}
