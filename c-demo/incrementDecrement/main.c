#include <stdio.h>

int main() {
  int pizzasToEat = 100;
  //pizzasToEat = pizzasToEat + 1;
  int output = pizzasToEat++; // Unary operator

  printf("Output: %i\n", output);
  printf("Pizzas to eat: %i\n", pizzasToEat);

  return 0;
}