#include <stdio.h>

int main() {
  int pizzasToEat = 100;
  
  printf("Pizzas to eat: %i\n", pizzasToEat);

  pizzasToEat += 100;

  printf("Pizzas to eat: %i\n", pizzasToEat);

  pizzasToEat -= 100;

  printf("Pizzas to eat: %i\n", pizzasToEat);

  pizzasToEat *= 2;

  printf("Pizzas to eat: %i\n", pizzasToEat);

  pizzasToEat /= 4;

  printf("Pizzas to eat: %i\n", pizzasToEat);

  pizzasToEat %= 5;

  printf("Pizzas to eat: %i\n", pizzasToEat);  

  return 0;
}