#include "stdio.h"

int main() {
  int slices;
  printf("How many slices have you eaten? >>>: ");
  scanf("%d", &slices);

  switch(slices)
  {
    case 1:
    case 2:
    case 3:
      printf("You did good!\n");
      break;
    case 4:
      printf("You did ok!\n");
      break;
    default:
      printf("You really like slices!\n");
      break;
  }

  return 0;
}