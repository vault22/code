#include <stdio.h>

int main() {
  char ascii;

  printf("Enter a character >>>: ");
  scanf("%c", &ascii);
  printf("The integer number for the entered character is: %i\n", ascii);

  int integer;
  printf("Enter an integer between 0 - 127 >>>: ");
  scanf("%i", &integer);
  printf("The ASCII character for the entered integer is: %c\n", integer);

  // Math with ascii
  char math = 'A' + '\t';
  printf("A(65) + \\t(9) = %c(%d)\n", math, math);

  return 0;
}