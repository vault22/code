// Game to guess a number between 0-5.
// If the user guesses correct, they win.
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>

int main(){
  int maxValue;
  printf("Enter the max value for this round (i.e 15 to guess a number between 0-15) >>>: ");
  scanf("%d", &maxValue);

  // Generate a pseudo random number
  // Seed - Computer clock
  srand(time(NULL));
  // Set random number between 0-maxValue
  int randomNumber = rand() % maxValue+1;

  printf("Guess a number between 0-%d >>>: ", maxValue);
  int guess;
  scanf("%d", &guess);

  if (guess == randomNumber)
  {
    printf("You win!\n");
  }
  else
  {
    printf("Incorrect! Try again.\n");
    return 1;
  }

  printf("Thank you for playing!\n");
  return 0;
}