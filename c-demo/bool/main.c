#include <stdio.h>
#include <stdbool.h>

int main() {

  bool isTrue = true;
  bool isFalse = false;

  printf("Is boolean true (1 is yes, 0 is no)? %i\n", isTrue);

  printf("%i\n", isTrue + 10);

  return 0;
}