#include <stdio.h>
#include <stdbool.h>

int main() {
  bool lovesBooch = true;
  int temp;
  printf("Do you like booch? 1 is true, 0 is false: ");
  scanf("%d", &temp);
  lovesBooch = temp;

  if(lovesBooch)
  { 
    printf("Enjoy fermented drinks!\n");
  }

  bool boochIsFinished = false;
  printf("How many days have the booch been brewing for? >>>: ");
  scanf("%d", &temp);

  // Booch is finished after > 3 days
  if(temp > 3) 
  {
    printf("Booch is ready to drink!\n");
  } 
  else
  {
    printf("Gotta sit for a while longer to carbonate...\n");
  }

  return 0;
}