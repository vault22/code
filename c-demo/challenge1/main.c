#include <stdio.h>
#include <math.h>

int main() {
  double a;
  double b;
  
  printf("Enter side a of triangle: ");
  scanf("%lf", &a);

  printf("Enter side b of triangle: ");
  scanf("%lf", &b);

  double cSquared = sqrt((a * a) + (b * b));

  printf("The hypothenus of the triangle is: %f\n", cSquared);

  return 0;
}