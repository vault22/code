#include <stdio.h>

int main() {
  int radius; // Distance from outside to center of circle  
  const int pi = 3.14159;
  printf("Enter radius of circle: ");
  scanf("%i", &radius); // Address-of operator, due to scanf changing
                       //  radius variable

  printf("The given radius is: %i\n",radius); // Doesn't change variable, so don't need &

  double area = pi * (radius * radius);

  printf("The area of a circle with the radius %i is: %f\n", radius, area);
    
  return 0;
}