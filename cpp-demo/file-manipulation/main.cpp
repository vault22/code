#include "process.h"
#include <iostream>
#include <fstream>

int main() {
  std::cout << "Enter the name of a file to write to >>>: ";
  std::string fileName;
  std::cin >> fileName;

  processFile(fileName);

  return 0;
}
