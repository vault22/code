#include "open.h"
bool handleFileOpening(std::ofstream& fileName) {
  if (fileName.is_open()) {
    std::cout << "Opening file success\n";
    return true;
  } 
  else {
    std::cout << "Opening file failure\n";
    return false;
  }
}