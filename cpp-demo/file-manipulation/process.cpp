#include "process.h"
#include "open.h"
#include "write.h"
int processFile(std::string fileName){
  // Create output stream on working file
  std::ofstream workingFile;
  workingFile.open(fileName, std::ios::app);
 
  // If opening file works, write to the file
  if (handleFileOpening(workingFile)) {
     writeToFile(workingFile, "writing...\n");
  } 
  // Otherwise print error and return.
  else {
    std::cerr << "Error opening file... \n";
    return 1;
  }
  
  workingFile.close();
  return 0;
}