#include <iostream>

int main() {
  int x = 10;
  //int y = x++; // Sets y to x, then adds 1 to x. Y is still 10.
  int z = ++x; // Increments x first, so z and x is 11.
  
  std::cout << x;
  
  return 0;
}