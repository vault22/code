#include <iostream>

// z = x + 10 / 3 * y
// x is 10
// y is 5
// z is 1.3 

int main() {
  double x = 10;
  double y = 5;
  double z = (x + 10) / (3 * y);

  std::cout << z;

  return 0;
}