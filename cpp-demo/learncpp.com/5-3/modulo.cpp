#include <iostream>
#include <tuple>

int getNum() {
  std::cout << "Enter a number: ";
  int x {};
  std::cin >> x;
  
  return x;
}

int main() {
  int x { getNum() };
  int y { getNum() };

  std::cout << "The remainder is: " << x % y << std::endl;
  if ( x % y == 0 ) {
    std::cout << x << " is evenly divisible with " << y << std::endl;
  }
  else {
    std::cout << x << " is not evenly divisible with " << y << std::endl;
  }

  return 0;
}
