#include <iostream>
#include <cmath>

int getNum() {
  std::cout << "Enter a number: ";
  int x {};
  std::cin >> x;
  
  return x;
}

int raiseToPower(int base, int exponent) {
  double result { std::pow(base, exponent) };
  return result;
}

int main() {
  int x { getNum() };
  int y { getNum() };

  int z { raiseToPower(x, y) };

  std::cout << x << " to the power of " << y << " is " << z << std::endl;

  return 0;
}
