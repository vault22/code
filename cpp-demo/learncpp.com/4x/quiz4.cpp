#include <iostream>

double getTowerHeight() {
  std::cout << "Enter the height of the tower in meters: ";
  double height {};
  std::cin >> height;

  return height;
}


double calculateHeight(double towerHeight, int seconds) {
  // Earths gravity constant
  constexpr double gravity {9.8};

  // Set the current height to the top of the tower
  double distanceFallen { gravity * (seconds * seconds) / 2.0 };
  double currentHeight { towerHeight - distanceFallen };
 
  return currentHeight;
}

void printHeight(double currentHeight, int seconds) {
  // As long as the ball is above ground
  if (currentHeight >= 0) {
    // Print the height
    std::cout << "At " << seconds << " seconds, the ball is at height: " << currentHeight << std::endl; 
    
  }
  // Otherwise, the ball is on the ground
  else {
    std::cout << "At " << seconds << " seconds, the ball is on the ground." << std::endl;
  }
}

void calculateAndPrintHeight(double towerHeight, int seconds, int maxValue) {
  printHeight(calculateHeight(towerHeight, seconds), seconds);
}

int main() {
  int maxValue {5};
  double towerHeight {getTowerHeight()};
  for (int seconds{0}; seconds < maxValue+1; seconds++){
    calculateAndPrintHeight(towerHeight, seconds, maxValue);
  }
}
