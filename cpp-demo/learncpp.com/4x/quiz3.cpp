#include <iostream>

double takeNum() {
  std::cout << "\nenter a double value >>>: ";
  double x {};
  std::cin >> x;

  return x;
}

char getOp() {
  std::cout << "\nenter one of the following [ +, -, *, or / ] >>>: ";
  char op {};
  std::cin >> op;

  return op;

}

double getResult(double x, char op, double y) {
  if (op == '+') {
    return x + y;
  }
  else if (op == '-') {
    return x - y;
  }
  else if (op == '*') {
    return x * y;
  }
  else if (op == '/') {
    return x / y;
  }
  else {
    return 1;
  }
}

int main() {
  double num1 {takeNum()};
  double num2 {takeNum()};

  char op {getOp()};
  double result {getResult(num1, op, num2)};

  std::cout << num1 << " " << op << " " << num2 << " is " << result << std::endl;

  return 0;
}
