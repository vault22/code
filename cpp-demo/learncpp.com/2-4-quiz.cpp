#include <iostream>

int doubleNumber(int num) {
  return num * 2;
}

int main() {
  int num{};
  std::cout << "Enter a number >>>: ";
  std::cin >> num;
  std::cout << num << " doubled is: " << doubleNumber(num) << '\n';
}
