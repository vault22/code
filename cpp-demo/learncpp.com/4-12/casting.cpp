#include <iostream>

int main() {
  std::cout << "Enter a characrter >>>: ";
  char ch;
  std::cin >> ch;
  std::cout << "The character " << ch << " has the value of: " << static_cast<int>(ch) << std::endl;

  std::cout << typeid(ch).name() << std::endl;
}
