#include <cstdint>
#include <iostream>

int main() {
  std::int8_t myint{65};
  std::cout << myint << std::endl;

  std::cout << "After conversion: " << static_cast<int>(myint) << std::endl;

  return 0;
}
