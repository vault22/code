#include <iostream>
#include <string_view>
#include <string>



std::string getNum()
{
  std::cout << "Enter a string: ";
  std::string inputStr{};
  std::getline(std::cin >> std::ws, inputStr);


  return inputStr;
}

constexpr int greater(int x, int y)
{
  return (x > y ? x : y);
}

int main()
{
  std::string_view x {getNum()};
  std::string_view y {"magic string"};
  std::string_view z {"You win!"};
  std::cout << (( x == y ) ? z : x );

  constexpr int w { 5 };
  constexpr int n { 10 };
  std::cout << greater(w, n) << " is greater!";

  return 0;
}
