#include <iostream>
#include <string>

int main() {
  std::cout << "enter your name: ";
  std::string name{};
  std::getline(std::cin >> std::ws, name);

  std::cout << "enter favorite food: ";
  std::string food{};
  std::getline(std::cin >> std::ws, food);

  std::cout << "your name is " << name << " and your favorie food is " << food << std::endl;

  return 0;
  
}
