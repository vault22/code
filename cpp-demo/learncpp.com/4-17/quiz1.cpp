#include <iostream>
#include <string>
#include <iterator>

int main() {
  std::cout << "Enter your name: ";
  std::string name {};
  std::cin >> name;
  std::cout << "Enter your age: ";
  int age {};
  std::cin >> age;
  std::cout << "your age + length of name is: " << age + std::ssize(name) << std::endl;

  return 0;
}
