#include <iostream>
#include "io.h"

int readNumber() {
  // get user input
  std::cout << "Enter an integer: ";
  int x {};
  std::cin >> x;
  
  return x;
}

void writeAnswer(int x) {
  // calculate answer
  std::cout << "The answer is " << x << '\n';
}
