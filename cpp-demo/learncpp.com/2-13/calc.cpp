#include <iostream>

int getUserInput() {
  std::cout << "Enter an integer >>>: ";
  int input{};
  std::cin >> input;

  return input;
}

int main() {
  // Get first number
  int value{ getUserInput() };
  std::cout << value << '\n'; // Debug code, will be removed

  // getMathOperator();

  // Get second number
  //getUserInput();
  
  //Calculate result
  //calculateResult();

  return 0;
}