#include <iostream>
#include <string>
#include <string_view>

int main() {
  using namespace std::string_literals;
  using namespace std::string_view_literals;

  std::cout << "foo\n";
  std::cout << "goo\n"s;
  std::cout << "moo\n"sv;

  return 0;
}
