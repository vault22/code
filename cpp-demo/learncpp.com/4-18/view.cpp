#include <iostream>
#include <string_view>

void printStrView(std::string_view str) {
  std::cout << str << std::endl;
}

int main() {
  std::string_view s{ "Hello, world" };
  printStrView(s);

  printStrView("Hello");

  return 0;
}
