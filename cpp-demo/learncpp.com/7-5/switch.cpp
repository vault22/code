#include <iostream>

void printDigitName(int x)
{
  switch(x)
  {
    case 1:
      std::cout << "One";
      break;
    case 2:
      std::cout << "Two";
      break;
    case 3: 
      std::cout << "Three";
      //  Intentional fallthrough attribute
      [[fallthrough]]; 
    case 4:
      std::cout << "Four";
      break;
    case 5:
    {
      int x {5};
      std::cout << x << std::endl;
      break;
    }
    default:
      std::cout << "Unknown";
      break;
  }
}

int main() 
{
  printDigitName(5);
  std::cout << std::endl;
}
