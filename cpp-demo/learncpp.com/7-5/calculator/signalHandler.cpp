#include <csignal>
#include <iostream>
#include "signalHandler.h"
void signalHandler(int signal)
{
  if (signal == 2) 
  {
    std::cout << "\nCtrl-C keypress detected, exiting calculator now...";
    std::exit(signal);
  }
  else
  {
    std::cout << "Signal " << signal << " recieved. \n";
    std::exit(signal);
  }
}