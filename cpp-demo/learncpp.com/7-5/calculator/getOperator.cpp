#include <iostream>
#include "getOperator.h"
char getOperator() 
{
  while (true)
  {
    std::cout << "Enter operator >>>: ";
    char op{};
    std::cin >> op;
  
    switch (op)
    {
      case '+':
      case '-':
      case '/':
      case '*':
        return op;
      default:
        std::cout << "Opsie woopsie, that input is invalid! Try again...\n";
    }
  }
}