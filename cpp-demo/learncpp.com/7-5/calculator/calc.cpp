#include <iostream>
#include <csignal>
#include <cstdint>
#include <limits>
#include "signalHandler.h"
#include "calculate.h"
#include "getOperator.h"
#include "ignoreLine.h"
#include "getInteger.h"

int main()
{
  std::signal(SIGINT, signalHandler);

  while (true) {
    //  Get first integer
    int x {getInteger()};

    //  If user enters 0, exit program
    if (x == 0)
    {
      break;
    }

    //  Get mathematical operator
    char op {getOperator()};

    //  Get second integer
    int y {getInteger()};

    //  Calculate result based on previous functions
    std::int64_t ans{calculate(x, op, y)};

    

    //  If calculation succeeded without errors
    if (ans != EXIT_FAILURE)
    {
      std::cout << ans << '\n';  
    }
    else 
    {
      std::cout << "\nCalculator exited with failure status EXIT_FAILURE";
    }  
  }
  std::cout << "Program is exiting...";
}
