#include <cstdint>
#include <iostream>
#include "calculate.h"
std::int64_t calculate(std::int64_t x, char op, std::int64_t y)
{
  switch(op)
  {
    case '+':
      return x + y;
    case '-':
      return x - y;
    case '/':
      return x / y;
    case '*':
      return x * y;
    case '%':
      return x % y;
    default:
      std::cout << "\ncalculate(): Unhandled case (invalid operator)\n";
      return EXIT_FAILURE;
  } 
}