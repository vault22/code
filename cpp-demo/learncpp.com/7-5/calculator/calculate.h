#ifndef CALCULATE_H
#define CALCULATE_H
#include <cstdint>
std::int64_t calculate(std::int64_t x, char op, std::int64_t y);
#endif