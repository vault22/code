#include <iostream>
#include <limits>
#include "ignoreLine.h"
void ignoreLine()
{
  std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
}
