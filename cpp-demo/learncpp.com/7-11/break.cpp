#include <iostream>

int breakOrReturn()
{
  while (true)
  {
    std::cout << "Enter 'b' to break or 'r' to return >>>: ";
    char ch{};
    std::cin >> ch;

    if(ch == 'b')
    {
      break;
    }
    if(ch == 'r')
    {
      return 1;
    }

  }

  std::cout << "Broke out of loop\n";
  return 0;
}

int main() 
{
  int returnCode{breakOrReturn()};
  std::cout << "Function breakOrReturn() returned " << returnCode << '.';
}
