#include <iostream>

int getNum() 
{
  std::cout << "Enter a number: ";
  int x {};
  std::cin >> x;

  return x;
}

int main() 
{
  int num1 {getNum()};
  int num2 {getNum()};
  std::cout << ((num1 > num2) ? num1 : num2) << " is largest\n";

  return 0;
}
