#include <iostream>

int main()
{
  constexpr bool inBigRoom { false };
  constexpr int classSize { inBigRoom ? 30 : 20 };
  std::cout << "The classroom size is: " << classSize << '\n';

  return 0;
}
