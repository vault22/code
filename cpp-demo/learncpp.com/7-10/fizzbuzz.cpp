#include <iostream>

void fizzbuzz(int count)
{
  for(int currentNum{ 1 }; currentNum <= count; ++currentNum)
  {
    if (currentNum % 3 == 0 && currentNum % 5 == 0) 
    {
      std::cout << "Fizzbuzz\n";
    }
    else if (currentNum % 3 == 0)
    {
      std::cout << "Fizz\n";
    }
    else if (currentNum % 5 == 0)
    {
      std::cout << "Buzz\n";
    }
    else if (currentNum % 7 == 0) 
    {
      std::cout << "pop\n";
    }
    else {
      std::cout << currentNum << '\n';
    }
  }
}

int main()
{
  std::cout << "Enter max count: ";
  int count{};
  std::cin >> count;
  fizzbuzz(count);
  return 0;
}
