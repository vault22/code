#include <iostream>

int sumTo(int x) 
{
  int total{ 0 };
  for(int count{ 0 }; count <= x; ++count)
  {
    total += count;
  }

  return total;
}

int main()
{
  std::cout << "Enter an integer: ";
  int x {};
  std::cin >> x;

  int z {sumTo(x)};
  std::cout << z;
  
}
