#include <iostream>

int main() {
  int x {1};
  int y {1};
  int z {x + y};

  std::cout << z << std::endl;

  return 0;
}