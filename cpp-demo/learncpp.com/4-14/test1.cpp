#include <iostream>

int main() {
  const int x {1};
  const int y {1};
  const int z {x + y};

  std::cout << z << std::endl;

  return 0;
}
