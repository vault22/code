#include <iostream>

int getNumber() {
  std::cout << "Enter a number >>>: ";
  int y{};
  std::cin >> y;

  return y;
}

int main() {
  int x{1};
  int y{getNumber()};
  int z{ x + y };

  std::cout << x << " + " << y << " = " << z << std::endl;

  return 0;
}
