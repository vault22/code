#include <iostream>
#include "getOperator.h"
char getOperator() 
{
  while (true)
  {
  
  std::cout << "Special operators: " << '\n' 
            << "1. (N)egative Factorial" << '\n'
            << "2. (D)ivisional Factorial" << '\n'
            << "3. (S)um to" << '\n';
  std::cout << "Enter operator >>>: ";

    char op{};
    std::cin >> op;
  
    switch (op)
    {
      case '+':
      case '-':
      case '/':
      case '*':
      case '!':
      case 'n':
      case 's':
      case 'd':
        return op;
      default:
        std::cout << "Opsie woopsie, that input is invalid! Try again...\n";
    }
  }
}