int calcFactorial(int value)
{
  int total { value };
  for (int num { value - 1}; num > 0; num--)
  {
    total *= num;
  }
  return total;
}