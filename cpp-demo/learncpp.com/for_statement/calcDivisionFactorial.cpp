double calcDivisionFactorial(double value)
{
  double total { value };
  for (double num { value - 1 }; num > 0; --num)
  {
    total /= num;
  }

  return total;
}
