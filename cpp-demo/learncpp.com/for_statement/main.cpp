#include <iostream>
#include "getInteger.h"
#include "getOperator.h"
#include "calculate.h"
#include "ignoreLine.h"
#include "printResult.h"
#include "calcNegTo.h"
#include "calcDivFactorial.h"
#include "calcSumTo.h"

int main()
{
  while (true)
  {
    int x { getInteger() };
    char op { getOperator() };
    if (op != '!' && op != 'n' && op != 'd')
    {
      int y { getInteger() };
      std::int64_t result { calculate(x, op, y) };
      printResult(result);
    }
    else {
      std::int64_t result { calculate(x, op, 0) };
      printResult(result);
    }  
  }
}