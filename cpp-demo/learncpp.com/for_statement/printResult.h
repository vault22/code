#ifndef PRINTRESULT_H
#define PRINTRESULT_H
#include <cstdint>
#include <iostream>

void printResult(std::int64_t result);
#endif