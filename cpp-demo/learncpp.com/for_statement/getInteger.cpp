#include <iostream>
#include "ignoreLine.h"
int getInteger()
{
  while (true)
  {
    //  Set the firstRun variable to true initially
    static bool firstRun {true};

    if (firstRun) 
    {
      std::cout << "Enter an integer or 0 to quit >>>: ";
      //  No longer first time running getInteger()
      firstRun = false;
    }
    else 
    {
      std::cout << "Enter another integer >>>: ";
    }

    int x{};
    //  Grab the integer the user supplied
    std::cin >> x;

    if(!std::cin && !std::cin.eof())
    {
      // Handle failure
      std::cin.clear();
      ignoreLine();
    }
    else if (std::cin.eof())
    {
      exit(0);
    }
    else
    {
      ignoreLine();
      //  Return the user supplied integer
      return x;  
    }  
  }
}
