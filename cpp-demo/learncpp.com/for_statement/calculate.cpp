#include <cstdint>
#include <iostream>
#include "calculate.h"
#include "calcFactorial.h"
#include "calcNegTo.h"
#include "calcDivFactorial.h"

std::int64_t calculate(std::int64_t x, char op, std::int64_t y)
{
  switch(op)
  {
    case '+':
      return x + y;
    case '-':
      return x - y;
    case '/':
      return x / y;
    case '*':
      return x * y;
    case '%':
      return x % y;
    case '!':
      return (calcFactorial(x));
    case 'n':
      return calcNegTo(x);
    case 'd':
      return calcDivisionFactorial(x);
    default:
      std::cout << "\ncalculate(): Unhandled case (invalid operator)\n";
      return EXIT_FAILURE;
  } 
}