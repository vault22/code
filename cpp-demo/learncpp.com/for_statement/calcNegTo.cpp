int calcNegTo(int value)
{
  int total { value };
  for (int previousValue { value - 1 }; previousValue >= 0; --previousValue)
  {
   total -= previousValue;
  }
  
  return total;
}