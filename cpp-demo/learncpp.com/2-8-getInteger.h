#ifndef GETINTEGER_CPP
#define GETINTEGER_CPP
#include <iostream>

int getInteger()
{
	std::cout << "Enter an integer: ";
	int x{};
	std::cin >> x;
	return x;
}
#endif
