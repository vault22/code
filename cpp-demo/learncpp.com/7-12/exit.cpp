#include <iostream>
#include <cstdlib>

void cleanup()
{
  std::cout << "Cleanup!\n";
}

int main()
{
  std::atexit(cleanup);

  std::cout << 1 << '\n';

  std::exit(0);
}
