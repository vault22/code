#include "game.h"
#include <iostream>
#include <string>
#include <cstdlib>

bool playGame(int guesses)
{
  int ceilingValue;
  int correctNumber;

  // Seed the random number using the clock
  srand(time(NULL));

  // Set a random number between 0 to user chosen number unless impossible mode
  if(guesses == 1)
  {
    // Random number between 0-99
    correctNumber = rand() % 100;
  }
  else
  {
    std::cout << 
      "Enter the ceiling value for the random number " << 
      "(i.e 11 will generate a number between 0-10)\n\n" <<
      ">>>: ";
    std::cin >> ceilingValue;
    
    correctNumber = rand() % ceilingValue;  
  }

  int guessCount = 0;
  
  std::cout << "\nPlaying game...\n\n";

  while(guessCount < guesses)
  {
    std::cout << "Your total guesses are: " << guesses << ".\n\n";
    std::cout << "Your remaining guesses are: " << guesses-guessCount << ".\n\n";
    std::cout << "Guess a number >>>: ";

    int guess;
    std::cin >> guess;
    if(guess == correctNumber)
    {
      return true;
    }
    else if(guess < correctNumber)
    {
      std::cout << "A bit higher...\n\n";
    }
    else
    {
      std::cout << "A bit lower...\n\n";
    }
    guessCount++;
    
  }
   return false; 
 }
