#include <iostream>
#include <string>

bool playGame(int guesses)
{
  std::cout << "Playing game...\n\n";

  int correctNumber = 42;
  std::cout << "You get " << guesses << " guesses.\n\n";
  std::cout << "Guess a number >>>: ";

  int guess;
  std::cin >> guess;
  if(guess == correctNumber)
  {
    return true;
  }
  return false;
  
}

int main() {
  std::cout << "Do you want to play a game? (y/n) >>>: ";
  char response;
  response = std::tolower(getchar());
  std::cout << "You entered: " << response << std::endl << std::endl;
  
  if(response == 'y')
  {
    bool beatGame;  
    bool wonGame;

    std::cout << 
      "Have you beaten the game previously? 0: no, 1: yes\n\n>>>: ";
    std::cin >> beatGame;

    if(beatGame)
    {
      std::cout <<
        "You have beaten the game previously and can thus play extreme difficulty mode\n\n";
        wonGame = playGame(1);
    }
    else
    {
      std::cout <<
        "You have not beaten the game yet.\n\n";
    }
    
    std::cout 
    << 
      "Let's play a game then! \n"
    <<
      "What difficulty do you want to play with?"
    << 
      "\n0: easy"
    << 
      "\n1: Medium"
    <<
      "\n2:Hard"
    <<
      "\n3:Impossible\n\n"
    << 
      ">>>:";

    int difficulty;
    std::cin >> difficulty;
    switch(difficulty)
    {
      case 0: //easy
        wonGame = playGame(15);
        break;
      case 1: //medium
        wonGame = playGame(10);
        break;
      case 2: //hard
        wonGame = playGame(5);
        break;
      case 3: //impossible
        wonGame = playGame(1);
        break;
      default:
        std::cout << "Invalid difficulty option chosen!";
        break;
    }
    if(wonGame)
    {
      std::cout << "You won!\n\n";
    }
  }
  else if(response == 'n')
  {
    std::cout << "Okay then, no game...\n";
  } 
  else
  {
    std::cout << "I didn't quite understand that... Valid options are (y/n)\n";
  }
  
  system("read -n1 -r -p \"Press any key to continue...\" key");
  return 0;
}
