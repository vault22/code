#include <iostream>
// #include <string>
#include "game.h"

int main() {
  std::cout << "Do you want to play a game?\n\n" << 
  ">>>: ";
  char response = std::tolower(getchar());

  if (response == 'y')
  {
    bool beatGame = false; 
    bool wonGame = false;

    std::cout << 
      "Have you previously beat the game?" << 
      "\n0: no" <<
      "\n1: yes\n\n" <<
      ">>>: ";
    std::cin >> beatGame;

    if(beatGame)
    {
      std::cout << "Because you have previously beat the game, you can play in impossible mode.\n\n";
      wonGame = playGame(1);
    }
    else
    {
      std::cout << "You have not beaten the game yet.\n\n";
      std::cout << 
        "Let's play!\n\n" <<
        "What difficulty do you want to play?\n" <<
        "0: easy\n" <<
        "1: medium\n" <<
        "2: hard\n" <<
        ">>>: ";

      int difficulty;
      std::cin >> difficulty;
      switch (difficulty)
      {
        case 0:
          wonGame = playGame(10);
          break;
        case 1:
          wonGame = playGame(5);
          break;
        case 2:
          wonGame = playGame(3);
          break;
        default:
          std::cout << "Invalid difficulty, try again.\n";
      }
      
    }
    
    if(wonGame)
    {
      std::cout << "You won!\n";
    }
    else
    {
      std::cout << "You lost!\n";
    }
  }
  system("read -n1 -r -p \"Press any key to continue...\" key");
  return 0;
}
