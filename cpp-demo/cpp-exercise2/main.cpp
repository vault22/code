#include <iostream>

using namespace std;

int main() {
  float sales = 95000;
  float stateTax = (4.0/100) * sales;
  float countyTax = (2.0/100) * sales;
  float totalTax = (6.0/100) * sales;
  
  cout << "The stateTax is: "  << stateTax  << endl
       << "The countyTax is: " << countyTax << endl
       << "The totalTax is: "  << totalTax;
  

  return 0;
}
