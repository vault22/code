#include <iostream>
#include <memory>

int main() {
  std::cout << "Enter values for x and y: ";
  double x;
  double y;
  
  std::cin >> x >> y;
  
  std::out << x + y;

  return 0;
}
