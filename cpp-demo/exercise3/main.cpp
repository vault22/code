#include <iostream>
int main() {
  double tempInFarenheit;
  std::cout << "Enter temperature in Fahrenheit: ";
  std::cin >> tempInFarenheit;

  double tempInCelsius = (tempInFarenheit - 32) * (5.0/9);

  std::cout << "Temperature in Celsius is: " << tempInCelsius;

  return 0;
}
