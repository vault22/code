#include <iostream>
#include <cmath>

int main() {
  double radius;
  const double pi = 3.14;
  std::cout << "Enter radius of a circle: ";
  std::cin >> radius;

  double circleArea = pi * std::pow(radius, 2);

  std::cout << "Area of circle is: " << circleArea;

  return 0;
}
