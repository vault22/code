#pragma once
class Game
{
public:
  Game();
  virtual ~Game();

  // operators

  //functions
  void mainMenu();

  // Accessors
  inline bool getPlaying() const { return this->playing; };

private:
  int choice;
  bool playing;
};