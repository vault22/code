#include <iostream>
#include <algorithm>
#include <string>

std::string no_space(const std::string& x)
{  
  x.erase(std::remove(x.begin(), x.end(), ' '), x.end());
  return "";
}