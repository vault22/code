package main

import (
	"fmt"
	"net/http"
)

type pair struct {
	x int
	y int
}

func startServ() {
	go func() {
		err := http.ListenAndServe(":1337", pair{})
		fmt.Println(err)
	}()

}
