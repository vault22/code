package calc

import (
	"testing"
)

func TestDivide(t *testing.T) {
	x, y := 6, 2
	expect := 3
	actual := Divide(x, y)
	if actual != expect {
		t.Errorf("Divide(%d,%d) returned: %d, expected: %d", x, y, actual, expect)
	}
}

func TestSquareNum(t *testing.T) {
	input := 2
	expect := 4
	actual := squareNum(input)
	if actual != expect {
		t.Errorf("squareNum(%d) returned: %d, expected: %d", input, actual, expect)
	}
}

func TestSumSlice(t *testing.T) {
	input := []int{1, 2, 3}
	expect := 6
	actual := SumSlice(input)
	if actual != 6 {
		t.Errorf("SumSlice([]int{1,2,3} returned %d, expected %d.)", actual, expect)
	}
}
