package calc

func Divide(x int, y int) int {
	return x / y
}

func SumSlice(slice []int) int {
	sum := 0
	for _, value := range slice {
		sum += value
	}
	return sum
}

func squareNum(num int) int {
	return num * num
}
