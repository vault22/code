package convert

import "testing"

// Equal tells whether a and b contain the same elements.
// A nil argument is equivalent to an empty slice.
func compareSlice(a, b []int) bool {
	if len(a) != len(b) {
		return false
	}
	for i, v := range a {
		if v != b[i] {
			return false
		}
	}
	return true
}

func TestNumToSlice(t *testing.T) {
	input := 123
	expect := []int{1, 2, 3}
	actual := NumToSlice(input)
	if compareSlice(expect, actual) != true {
		t.Errorf("NumberToSlice(123) returned %v, expected %v", actual, expect)
	}

}
