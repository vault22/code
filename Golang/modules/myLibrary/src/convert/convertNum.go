package convert

import (
	"strconv"
)

func NumToSlice(num int) []int {
	strNum := strconv.Itoa(num)

	digits := make([]int, len(strNum))

	for i, char := range strNum {
		digit, _ := strconv.Atoi(string(char))
		digits[i] = digit
	}

	return digits
}
