package main

import (
	"bufio"
	"fmt"
	"os"
)

func main() {
	scanner := bufio.NewScanner(os.Stdin)

	fmt.Println("Press ENTER")
	scanner.Scan()

	fmt.Println("Hello, world")
}
