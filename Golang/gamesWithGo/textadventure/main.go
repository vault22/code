package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type storyNode struct {
	text    string
	choices *choices
}

type choices struct {
	cmd         string
	description string
	nextNode    *storyNode
	nextChoice  *choices
}

func (node *storyNode) addChoice(cmd string, description string, nextNode *storyNode) {
	choice := &choices{cmd, description, nextNode, nil}

	if node.choices == nil {
		node.choices = choice
	} else {
		currentChoice := node.choices
		for currentChoice.nextChoice != nil {
			currentChoice = currentChoice.nextChoice
		}
		currentChoice.nextChoice = choice
	}
}

func (node *storyNode) render() {
	fmt.Println(node.text)
	currentChoice := node.choices
	for currentChoice != nil {
		fmt.Println(currentChoice.cmd, ":", currentChoice.description)
		currentChoice = currentChoice.nextChoice
	}
}

func (node *storyNode) executeCmd(cmd string) *storyNode {
	currentChoice := node.choices
	for currentChoice != nil {
		if strings.ToLower(currentChoice.cmd) == strings.ToLower(cmd) {
			return currentChoice.nextNode
		}
		currentChoice = currentChoice.nextChoice
	}
	fmt.Println("Sorry, I didn't understand that.")
	return node
}

var scanner *bufio.Scanner

func (node *storyNode) play() {
	node.render()
	if node.choices != nil {
		scanner.Scan()
		node.executeCmd(scanner.Text()).play()
	}
}

func main() {
	scanner = bufio.NewScanner(os.Stdin)

	start := storyNode{text: `
			You are in a large cave, deep underground.
			You see two passages leading deeper into the cave. 
			One heads down, and the other heads forwards.`}

	darkRoom := storyNode{text: "It is pitch black, you cannot see a thing!"}
	darkRoomLit := storyNode{text: "The room lights up!"}

	crackInWall := storyNode{text: "You squeeze through a tiny crack in the wall..."}

	treasure := storyNode{text: "You arrive at a chamber full of gold coins!"}

	start.addChoice("N", "Go north", &darkRoom)
	start.addChoice("S", "Go south", &darkRoom)
	start.addChoice("E", "Go east", &treasure)
	start.addChoice("W", "Go west", &crackInWall)

	crackInWall.addChoice("E", "Go back", &start)

	darkRoom.addChoice("S", "Go back", &start)
	darkRoom.addChoice("N", "Go back", &start)
	darkRoom.addChoice("L", "Turn on lantern", &darkRoomLit)

	darkRoomLit.addChoice("E", "Go east", &treasure)

	start.play()

	fmt.Println()
	fmt.Println("The end!")
}
