package main

import (
	"fmt"
)

type position struct {
	x float32
	y float32
}

type enemy struct {
	name     string
	health   int
	position position
}

func locateEnemy(b enemy) {
	x := b.position.x
	y := b.position.y
	fmt.Println("(", x, ",", y, ")")
}

func main() {

	pos := position{x: 4, y: 10}

	fmt.Println(pos.x)

	b := enemy{"Hemlin", 100, pos}

	locateEnemy(b)
}
