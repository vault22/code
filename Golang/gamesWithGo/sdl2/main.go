package main

import (
	"fmt"
	"github.com/veandco/go-sdl2/sdl"
)

func main() {
	if err := sdl.Init(sdl.INIT_EVERYTHING); err != nil {
		fmt.Println("An error occured:")
		panic(err)
	}
	defer sdl.Quit()

	window, err := sdl.CreateWindow("Windowboi", sdl.WINDOWPOS_UNDEFINED, sdl.WINDOWPOS_UNDEFINED,
		800, 600, sdl.WINDOW_SHOWN)
	if err != nil {
		fmt.Println("An error occurred:", err)
		return
	}

	defer window.Destroy()

	sdl.Delay(2000)

}
