package main

import (
	"fmt"
)

func sayHello(name ...string) {
	if len(name) == 0 {
		name = append(name, "world")
	}
	fmt.Println("Hello,", name)
}

func sayBye(name string) {
	fmt.Println("Goodbye,", name)
}

func main() {
	sayHello()
	sayHello("test")
	sayBye("test")
}
