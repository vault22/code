package main

import (
	"fmt"
)

type position struct {
	x float32
	y float32
}

type enemy struct {
	name     string
	health   int
	position position
}

func locateEnemy(b *enemy) {
	x := b.position.x
	y := b.position.y
	fmt.Println("(", x, ",", y, ")")
}

func addOne(num *int) {
	*num = *num + 1
}

func main() {

	x := 5
	fmt.Println(x)

	xPtr := &x

	fmt.Println(xPtr)

	addOne(xPtr)

	fmt.Println((x))

	pos := position{x: 4, y: 10}

	b := enemy{"Hemlin", 100, pos}

	locateEnemy(&b)

}
