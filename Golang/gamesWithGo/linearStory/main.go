package main

import (
	//"bufio"
	"fmt"
	//"os"
)

type storyPage struct {
	text     string
	nextPage *storyPage
}

func (page *storyPage) playStory() {
	for page != nil {
		fmt.Println(page.text)
		page = page.nextPage
	}
}

func (page *storyPage) addToEnd(text string) {
	for page.nextPage != nil {
		page = page.nextPage
	}
	page.nextPage = &storyPage{text, nil}
}

func (page *storyPage) addAfter(text string) {
	newPage := &storyPage{text, page.nextPage}
	page.nextPage = newPage
}

func main() {
	//scanner := bufio.NewScanner(os.Stdin)

	page1 := storyPage{"It was a dark and stormy night...", nil}
	page1.addToEnd("You are in a dark room, you need to find the sacred book.")
	page1.addToEnd("You encounter an enemy!")

	page1.addAfter("Testing addAfter")
	page1.playStory()

}
