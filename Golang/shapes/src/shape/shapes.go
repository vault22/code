package shape

import (
	"math"
)

type Circle struct {
	Radius float64
}

type Rectangle struct {
	Length float64
	Width  float64
}

func (c Circle) Perimeter() float64 {
	return math.Pi * 2 * c.Radius
}

func (r Rectangle) Perimeter() float64 {
	return 2 * (r.Length + r.Width)
}

func (r Rectangle) Area() float64 {
	return (r.Length * r.Width) / 2
}

func (c Circle) Area() float64 {
	return math.Pi * (c.Radius * c.Radius)
}
