package main

import (
	"fmt"
	"main/src/shape"
)

type border interface {
	Perimeter() float64
}

type surface interface {
	Area() float64
}

type surfaceBorder interface {
	border
	surface
}

type square struct {
	shape.Rectangle
}

func newSquare(side float64) square {
	return square{shape.Rectangle{Length: side, Width: side}}
}

func main() {
	shapes := make([]surfaceBorder, 3)

	shapes[0] = shape.Rectangle{Length: 10, Width: 5}
	shapes[1] = newSquare(5)
	shapes[2] = shape.Circle{Radius: 3}

	for _, s := range shapes {
		printBorder(s)
		printArea(s)

		switch c := s.(type) {
		case shape.Circle:
			fmt.Println("Radius is:", c.Radius)
		case shape.Rectangle:
			fmt.Println("Length is:", c.Length)
		}
	}
}

func printArea(s surface) {
	fmt.Println("The surface area is:", s.Area())
}

func printBorder(b border) {
	fmt.Println("Border length is:", b.Perimeter())
}
