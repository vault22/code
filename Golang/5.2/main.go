package main

import ()

func main() {
	var m map[string]int      // Nil, no storage
	p := make(map[string]int) // non-nil but empty

	a := p["the"] // returns 0
	b := m["the"] // same thing
	m["and"] = 1  // PANIC - nil map
	m = p
	m["and"]++    // OK, same map as p now
	c := p["and"] // returns 1
}
