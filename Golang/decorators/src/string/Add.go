package string

import "fmt"

// Decorators are common in other languages. Same can be done in Go
// with function literals that accept arguments.

// Takes a string (myString) and returns a function that takes two strings (before, after), and inserts myString between before and after.
func Concatenate(myString string) func(before, after string) string {
	return func(before, after string) string {
		return fmt.Sprintf("%s %s %s", before, myString, after) // new string
	}
}
