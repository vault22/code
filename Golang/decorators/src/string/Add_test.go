package string

import (
	"testing"
)

func TestAdd(t *testing.T) {
	input := "summer"
	before := "A beautiful"
	after := "day!"
	expected := "A beautiful summer day!"
	result := Add(input)(before, after)

	if result != expected {
		t.Errorf("alter.Add(\"summer\") returned %v, expected %v", result, expected)
	}
}
