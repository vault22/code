package main

import (
	"fmt"
	"main/src/string"
)

func main() {
	defer fmt.Println("End of day")

	season := string.Concatenate("summer")
	fmt.Println(season("A beautiful", "day!"))
}
