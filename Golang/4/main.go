package main

import (
	"fmt"
)

func isTrue(num int) string {
	if num == 0 {
		return "False"
	} else {
		return "True"
	}
}

func main() {
	fmt.Println("Start")
	fmt.Println(isTrue(5))
}
