package main

import (
	"fmt"
)

func inc(i int, c chan int) {
	c <- i + 1
}

func main() {
	c := make(chan int)

	go inc(0, c)
	go inc(10, c)
	go inc(80, c)

	fmt.Println(<-c, <-c, <-c)
}
