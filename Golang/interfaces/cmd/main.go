package main

import (
	"fmt"
)

type ninjaStaff struct {
	owner string
}

type ninjaSword struct {
	owner string
}

type ninjaStar struct {
	owner string
}

type ninjaWeapon interface {
	attack()
}

func attack(weapon ninjaWeapon) {
	weapon.attack()
}

func (ninjaSword) attack() {
	fmt.Println("slashing sword")
}

func (ninjaStar) attack() {
	fmt.Println("throwing ninja star")
}

func (ninjaStaff) attack() {
	fmt.Println("swinging staff")
}

func main() {
	weapons := []ninjaWeapon{ninjaStaff{"testOwner0"}, ninjaStar{"testOwner1"}, ninjaSword{"testOwner2"}}
	for _, weapon := range weapons {
		weapon.attack()
	}
}
