package kata

import (
	"fmt"
	"strconv"
)

func DigitalRoot(n int) int {
	strNum := strconv.Itoa(n)
	total := 0
	for _, char := range strNum {
		total += strconv.Atoi(string(char))
	}
}
