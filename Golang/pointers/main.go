package main

// When to use pointers?
// 1. When we need to update state.
// 2. When we need to optimize memory for large objects that are getting called A LOT.

import (
	"fmt"
)

type User struct {
	email    string
	userName string
	age      int
}

func (user *User) getEmail() string {
	return user.email
}

func (user *User) updateEmail(email string) {
	user.email = email
}

func main() {
	user := User{
		email: "agg@foo.com",
	}

	user.updateEmail("foo@bar.com")

	fmt.Println(user.getEmail())
}
