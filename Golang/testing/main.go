package main

import (
	"fmt"
)

func main() {

	arr := []string{"a", "b", "c"}
	for index, value := range arr {
		fmt.Println(index, value)
	}
	for i := 0; i < 11; i++ {
		fmt.Printf("(%d, %d)\n", i, i*i)
	}

	// vert := make(map[string]int)

	// vert["triangle"] = 2
	// vert["square"] = 3
	// vert["circle"] = 4

	// delete(vert, "square")

	// fmt.Println(vert)
}
