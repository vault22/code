package main

import (
	"fmt"
	"os"
)

func main() {
	num := 1
	for {
		fmt.Print(num, " ")

		//  If it's the 10th number
		if num%10 == 0 {
			//  Make a newline
			_, err := fmt.Scanln()
			if err != nil {
				fmt.Println(err)
				os.Exit(1)
			}
		}

		num++
	}
}
