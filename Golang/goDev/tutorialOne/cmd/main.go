package main

import (
	"fmt"
	"log"

	"example.com/greetings/src/greetings"
)

func main() {
	// Set properties of the predefined Logger, including
	// the log entry prefix and a flag to disable printing
	// the time, source file, and line number
	log.SetPrefix("greetings error: ")
	log.SetFlags(0)

	// A slice of names
	names := []string{"Foo", "Bar", "Moo"}

	// Request a greeting message
	msg, err := greetings.Hellos(names)
	if err != nil {
		log.Fatal(err)
	}

	// Get a greeting message and print it

	fmt.Println(msg)
}
